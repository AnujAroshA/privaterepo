//
//  ViewController.m
//  FFmpegTest
//
//  Created by Anuja on 6/30/15.
//  Copyright (c) 2015 AnujAroshA. All rights reserved.
//

#import "ViewController.h"
#import "avformat.h"
#import <libswscale/swscale.h>
#import <libavcodec/avcodec.h>

@interface ViewController ()
{
    bool initDone;
    int framesPerSec;
    int index;
    int inputAudioStreamIndex;
    int inputVideoStreamIndex;
    int numberOfFrames;
    int videoStream;
    int64_t inputEndtimeInt64;
    int64_t outputStartTimeInt64;
    NSString *editedVideoFilePath;
    NSMutableArray *srcPktArr;
    AVCodec *outputVideoCodec;
    AVCodec *outputAudioCodec;
    AVCodecContext *pCodecCtxOrig;
    AVCodecContext *pCodecCtx;
    AVFormatContext *inputFormatCtx;
    AVFormatContext *outputContext;
    AVOutputFormat *outputFormat;
    AVStream *inputAudioStream;
    AVStream *inputVideoStream;
    AVStream *outputAudioStream;
    AVStream *outputVideoStream;
}
@end

@implementation ViewController

#pragma mark - UIViewControler lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%s - %d", __PRETTY_FUNCTION__, __LINE__);
    
    srcPktArr = [[NSMutableArray alloc] init];
  
    outputStartTimeInt64 = 100;
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"%s - %d", __PRETTY_FUNCTION__, __LINE__);
    
    inputFormatCtx = NULL;
    
    /*
     *  Getting the raw video file path
     */
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"KeseMase" ofType:@".mp4"];
    
    if (filePath)
    {
        NSLog(@"%s - %d # File found", __PRETTY_FUNCTION__, __LINE__);
    }
    else
    {
        NSLog(@"%s - %d # File NOT found", __PRETTY_FUNCTION__, __LINE__);
    }
    
    /*
     *  av_find_input_format(const char *short_name)
     *
     *  Find AVInputFormat based on the short name of the input format.
     */
    AVInputFormat *inputFormat = av_find_input_format([@"mp4" UTF8String]);
    
    if (inputFormat)
    {
        NSLog(@"%s - %d # inputFormat identifed", __PRETTY_FUNCTION__, __LINE__);
    }
    else
    {
        NSLog(@"%s - %d # inputFormat NOT identifed", __PRETTY_FUNCTION__, __LINE__);
    }
    
    const char *utf8FilePath = [filePath UTF8String];
    NSLog(@"%s - %d # utf8FilePath = %s", __PRETTY_FUNCTION__, __LINE__, utf8FilePath);
    
    /*
     *  avformat_open_input(AVFormatContext **ps, const char *filename, AVInputFormat *fmt, AVDictionary **options)
     *
     *  Open an input stream and read the header. The codecs are not opened.
     */
    int openInputValue = avformat_open_input(&inputFormatCtx, utf8FilePath, inputFormat, nil);
    NSLog(@"%s - %d # openInputValue = %d", __PRETTY_FUNCTION__, __LINE__, openInputValue);
    
    if (openInputValue == 0)
    {
        NSLog(@"%s - %d # Can open the file", __PRETTY_FUNCTION__, __LINE__);
    }
    else
    {
        NSLog(@"%s - %d # Cannot open the file", __PRETTY_FUNCTION__, __LINE__);
        avformat_close_input(&inputFormatCtx);
    }
    
    /*
     *  Read packets of a media file to get stream information. 
     *
     *  avformat_find_stream_info(AVFormatContext *ic, AVDictionary **options)
     */
    int streamInfoValue = avformat_find_stream_info(inputFormatCtx, NULL);
    NSLog(@"%s - %d # streamInfoValue = %d", __PRETTY_FUNCTION__, __LINE__, streamInfoValue);
    
    if (streamInfoValue < 0)
    {
        NSLog(@"%s - %d # streamInfoValue Error", __PRETTY_FUNCTION__, __LINE__);
        avformat_close_input(&inputFormatCtx);
    }
    
    /*
     *  nb_streams : Number of Audio and Video streams of the input file
     */
    NSUInteger inputStreamCount = inputFormatCtx->nb_streams;
    NSLog(@"%s - %d # inputStreamCount = %d", __PRETTY_FUNCTION__, __LINE__, inputStreamCount);
    
    for(unsigned int i = 0; i<inputStreamCount; i++)
    {
        if(inputFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            NSLog(@"%s - %d # Found Video Stream", __PRETTY_FUNCTION__, __LINE__);
            inputVideoStreamIndex = i;
            inputVideoStream = inputFormatCtx->streams[i];
        }
        
        if(inputFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            NSLog(@"%s - %d # Found Audio Stream", __PRETTY_FUNCTION__, __LINE__);
            inputAudioStreamIndex = i;
            inputAudioStream = inputFormatCtx->streams[i];
        }
    }
    
    if(inputVideoStreamIndex == -1 && inputAudioStreamIndex == -1)
    {
        NSLog(@"%s - %d # Have not found any Video or Audio stream", __PRETTY_FUNCTION__, __LINE__);
    }
    
    /*
     *  Finding duration of the stream
     */
    if(inputFormatCtx->duration == AV_NOPTS_VALUE)
    {
        NSLog(@"%s - %d # Undefined timestamp value", __PRETTY_FUNCTION__, __LINE__);
        
        if(inputVideoStreamIndex != -1 && inputFormatCtx->streams[inputVideoStreamIndex])
        {
            if(inputFormatCtx->streams[inputVideoStreamIndex]->duration != AV_NOPTS_VALUE)
            {
                inputEndtimeInt64 = (inputFormatCtx->streams[inputVideoStreamIndex]->duration)/(inputFormatCtx->streams[inputVideoStreamIndex]->time_base.den/inputFormatCtx->streams[inputVideoStreamIndex]->time_base.num);
            }
        }
        else if(inputAudioStreamIndex != -1 && inputFormatCtx->streams[inputAudioStreamIndex])
        {
            if(inputFormatCtx->streams[inputAudioStreamIndex]->duration != AV_NOPTS_VALUE)
            {
                inputEndtimeInt64 = (inputFormatCtx->streams[inputAudioStreamIndex]->duration)/(AV_TIME_BASE);
            }
        }
    }
    else
    {
        NSLog(@"%s - %d # Defined timestamp value", __PRETTY_FUNCTION__, __LINE__);
        
        inputEndtimeInt64 = (inputFormatCtx->duration)/(AV_TIME_BASE);
    }
    NSLog(@"%s - %d # inputEndtimeInt64 = %lld", __PRETTY_FUNCTION__, __LINE__, inputEndtimeInt64);
    
    /*
     *  Finding out the frame rate
     */
    if(inputVideoStreamIndex != -1 && inputFormatCtx->streams[inputVideoStreamIndex])
    {
        if(inputFormatCtx->streams[inputVideoStreamIndex]->r_frame_rate.num != AV_NOPTS_VALUE && inputFormatCtx->streams[inputVideoStreamIndex]->r_frame_rate.den != 0)
        {
            framesPerSec =  (inputFormatCtx->streams[inputVideoStreamIndex]->r_frame_rate.num)/ (inputFormatCtx->streams[inputVideoStreamIndex]->r_frame_rate.den);
        }
    }
    else
    {
        framesPerSec = 25;
    }
    
    numberOfFrames = framesPerSec * (int) inputEndtimeInt64;
    NSLog(@"%s - %d # numberOfFrames = %d", __PRETTY_FUNCTION__, __LINE__, numberOfFrames);
    
    /*
     *  Seek to timestamp ts.
     *
     *  avformat_seek_file(AVFormatContext *s, int stream_index, int64_t min_ts, int64_t ts, int64_t max_ts, int flags)
     */
    if(avformat_seek_file(inputFormatCtx, inputAudioStreamIndex, INT64_MIN, outputStartTimeInt64, INT64_MAX, AVSEEK_FLAG_FRAME) < 0)
    {
        NSLog(@"%s - %d # Seek OK", __PRETTY_FUNCTION__, __LINE__);
    }
    else
    {
        NSLog(@"%s - %d # Seek ERROR", __PRETTY_FUNCTION__, __LINE__);
    }

    /*
     *  Creating output file path
     */
    NSArray *directoryPathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [directoryPathsArray objectAtIndex:0];
    NSString *outputFilePath = [NSString stringWithFormat:@"%@/Output.mov", documentsDirectory]; // Not working if we replace .avi with .mp4
    
    /*
     *  Return the output format in the list of registered output formats
     *  which best matches the provided parameters, or return NULL if
     *  there is no match.
     *
     *  av_guess_format(const char *short_name, const char *filename, const char *mime_type)
     */
     outputFormat = av_guess_format(NULL, [outputFilePath UTF8String], NULL);
    
    
    NSLog(@"%s - %d # outputFormat->name = %s", __PRETTY_FUNCTION__, __LINE__, outputFormat->name);
    
    if(outputFormat == NULL)
    {
        NSLog(@"%s - %d # outputFormat == NULL", __PRETTY_FUNCTION__, __LINE__);
    }
    else
    {
        /*
         *  Allocate an AVFormatContext.
         */
        outputContext = avformat_alloc_context();

        if(outputContext)
        {
            outputContext->oformat = outputFormat;      // The output container format.
            
            snprintf(outputContext->filename, sizeof(outputContext->filename), "%s", [outputFilePath UTF8String]);
        }
        else
        {
            NSLog(@"%s - %d # outputContext == NULL", __PRETTY_FUNCTION__, __LINE__);
        }
    }
    
    outputVideoCodec = outputAudioCodec = NULL;
    
    /*
     *  video_codec = default video codec
     */
    if(outputFormat->video_codec != AV_CODEC_ID_NONE && inputVideoStream != NULL)
    {
        /*
         *  Find a registered encoder with a matching codec ID.
         *
         *  avcodec_find_encoder(enum AVCodecID id)
         */
        outputVideoCodec = avcodec_find_encoder(outputFormat->video_codec);
        
        if(NULL == outputVideoCodec)
        {
            NSLog(@"%s - %d # Could Not Find Vid Encoder", __PRETTY_FUNCTION__, __LINE__);
        }
        else
        {
            NSLog(@"%s - %d # Found Out Vid Encoder", __PRETTY_FUNCTION__, __LINE__);
            
            /*
             *  Add a new stream to a media file.
             *
             *  avformat_new_stream(AVFormatContext *s, const AVCodec *c)
             */
            outputVideoStream = avformat_new_stream(outputContext, outputVideoCodec);
            
            if(NULL == outputVideoStream)
            {
                NSLog(@"%s - %d # Failed to Allocate Output Vid Strm", __PRETTY_FUNCTION__, __LINE__);
            }
            else
            {
                NSLog(@"%s - %d # Allocated Video Stream", __PRETTY_FUNCTION__, __LINE__);
                
                /*
                 *  Copy the settings of the source AVCodecContext into the destination AVCodecContext.
                 *
                 *  avcodec_copy_context(AVCodecContext *dest, const AVCodecContext *src)
                 */
                if(avcodec_copy_context(outputVideoStream->codec, inputFormatCtx->streams[inputVideoStreamIndex]->codec) != 0)
                {
                    NSLog(@"%s - %d # Failed to Copy Context", __PRETTY_FUNCTION__, __LINE__);
                }
                else
                {
                    outputVideoStream->sample_aspect_ratio.den = outputVideoStream->codec->sample_aspect_ratio.den;     // denominator
                    outputVideoStream->sample_aspect_ratio.num = inputVideoStream->codec->sample_aspect_ratio.num;    // numerator
                    NSLog(@"%s - %d # Copied Context 1", __PRETTY_FUNCTION__, __LINE__);
                    outputVideoStream->codec->codec_id = inputVideoStream->codec->codec_id;
                    outputVideoStream->codec->time_base.num = 1;
                    outputVideoStream->codec->time_base.den = framesPerSec*(inputVideoStream->codec->ticks_per_frame);
                    outputVideoStream->time_base.num = 1;
                    outputVideoStream->time_base.den = 1000;
                    outputVideoStream->r_frame_rate.num = framesPerSec;
                    outputVideoStream->r_frame_rate.den = 1;
                    outputVideoStream->avg_frame_rate.den = 1;
                    outputVideoStream->avg_frame_rate.num = framesPerSec;
                    outputVideoStream->duration = inputVideoStream->duration;
                }
            }
        }
    }
    
    if(outputFormat->audio_codec != AV_CODEC_ID_NONE && inputAudioStream != NULL)
    {
        outputAudioCodec = avcodec_find_encoder(outputFormat->audio_codec);
        
        if(NULL == outputAudioCodec)
        {
            NSLog(@"%s - %d # Could Not Find Out Aud Encoder", __PRETTY_FUNCTION__, __LINE__);
        }
        else
        {
            NSLog(@"%s - %d # Found Out Aud Encoder", __PRETTY_FUNCTION__, __LINE__);
            
            outputAudioStream = avformat_new_stream(outputContext, outputAudioCodec);
            
            if(NULL == outputAudioStream)
            {
                NSLog(@"%s - %d # Failed to Allocate Out Vid Strm", __PRETTY_FUNCTION__, __LINE__);
            }
            else
            {
                if(avcodec_copy_context(outputAudioStream->codec, inputFormatCtx->streams[inputAudioStreamIndex]->codec) != 0)
                {
                    NSLog(@"%s - %d # Failed to Copy Context", __PRETTY_FUNCTION__, __LINE__);
                }
                else
                {
                    NSLog(@"%s - %d # Copied Context 2", __PRETTY_FUNCTION__, __LINE__);
                    outputAudioStream->codec->codec_id = inputAudioStream->codec->codec_id;
                    outputAudioStream->codec->codec_tag = 0;
                    outputAudioStream->pts = inputAudioStream->pts;
                    outputAudioStream->duration = inputAudioStream->duration;
                    outputAudioStream->time_base.num = inputAudioStream->time_base.num;
                    outputAudioStream->time_base.den = inputAudioStream->time_base.den;
                }
            }
        }
    }
    
    if (!(outputFormat->flags & AVFMT_NOFILE))
    {
        /*
         *  Create and initialize a AVIOContext for accessing the resource indicated by url.
         *
         *  avio_open2(AVIOContext **s, const char *url, int flags, const AVIOInterruptCB *int_cb, AVDictionary **options)
         */
        if (avio_open2(&outputContext->pb, [outputFilePath UTF8String], AVIO_FLAG_WRITE, NULL, NULL) < 0)
        {
            NSLog(@"%s - %d # Could Not Open File", __PRETTY_FUNCTION__, __LINE__);
        }
    }
    
    /* Write the stream header, if any. */
    /*
     *  Allocate the stream private data and write the stream header to an output media file.
     *
     *  avformat_write_header(AVFormatContext *s, AVDictionary **options);
     */
    if (avformat_write_header(outputContext, NULL) < 0)
    {
        NSLog(@"%s - %d # Error Occurred While Writing Header", __PRETTY_FUNCTION__, __LINE__);
    }
    else
    {
        NSLog(@"%s - %d # Written Output header", __PRETTY_FUNCTION__, __LINE__);
        initDone = true;
    }
    
    [self generateClip];
}

#pragma mark - supportive methods

- (void)generateClip
{
    NSLog(@"%s - %d", __PRETTY_FUNCTION__, __LINE__);
    
    AVPacket pkt;
    AVPacket outpkt;
    int aud_pts = 0, vid_pts = 0, aud_dts = 0; // vid_dts = 0
    int last_vid_pts = 0;
    
    /*
     *  Return the next frame of a stream.
     *
     *  av_read_frame(AVFormatContext *s, AVPacket *pkt)
     */
    while(av_read_frame(inputFormatCtx, &pkt) >= 0 && (numberOfFrames-- > 0))
    {
        if(pkt.stream_index == inputVideoStreamIndex)
        {
            /*
             *  Rescale a 64-bit integer by 2 rational numbers.
             *
             *  av_rescale_q(int64_t a, AVRational bq, AVRational cq)
             */
//            NSLog(@"%s - %d # VideoStream PTS of the packet = %lld", __PRETTY_FUNCTION__, __LINE__, av_rescale_q(pkt.pts, inputVideoStream->time_base, inputVideoStream->codec->time_base));
//            NSLog(@"%s - %d # VideoStream DTS of the packet = %lld", __PRETTY_FUNCTION__, __LINE__, av_rescale_q(pkt.dts, inputVideoStream->time_base, inputVideoStream->codec->time_base));
            
            /*
             *  Initialize optional fields of a packet with default values.
             *
             *  av_init_packet(AVPacket *pkt)
             */
            av_init_packet(&outpkt);
            
            if(pkt.pts != AV_NOPTS_VALUE)
            {
                if(last_vid_pts == vid_pts)
                {
                    vid_pts++;
                    last_vid_pts = vid_pts;
                }
                outpkt.pts = vid_pts;

//                NSLog(@"%s - %d # ReScaled VID Pts = %lld", __PRETTY_FUNCTION__, __LINE__, outpkt.pts);
            }
            else
            {
                outpkt.pts = AV_NOPTS_VALUE;
            }
            
            if(pkt.dts == AV_NOPTS_VALUE)
            {
                outpkt.dts = AV_NOPTS_VALUE;
            }
            else
            {
                outpkt.dts = vid_pts;

//                NSLog(@"%s - %d # ReScaled VID Pts #2# = %lld", __PRETTY_FUNCTION__, __LINE__, outpkt.pts);
            }
            
            outpkt.data = pkt.data;
            outpkt.size = pkt.size;
            outpkt.stream_index = pkt.stream_index;
            outpkt.flags |= AV_PKT_FLAG_KEY;
            last_vid_pts = vid_pts;
            
            /*
             *  Write a packet to an output media file ensuring correct interleaving.
             *
             *  av_interleaved_write_frame(AVFormatContext *s, AVPacket *pkt)
             */
            if(av_interleaved_write_frame(outputContext, &outpkt) < 0)
            {
                NSLog(@"%s - %d # Failed Video Write", __PRETTY_FUNCTION__, __LINE__);
            }
            else
            {
                outputVideoStream->codec->frame_number++;
            }
            
            av_free_packet(&outpkt);
            av_free_packet(&pkt);
        }
        else if(pkt.stream_index == inputAudioStreamIndex)
        {
//            NSLog(@"%s - %d # AudioStream PTS of the packet = %lld", __PRETTY_FUNCTION__, __LINE__, av_rescale_q(pkt.pts, inputAudioStream->time_base, inputAudioStream->codec->time_base));
//            NSLog(@"%s - %d # AudioStream DTS of the packet = %lld", __PRETTY_FUNCTION__, __LINE__, av_rescale_q(pkt.dts, inputAudioStream->time_base, inputAudioStream->codec->time_base));
            
            av_init_packet(&outpkt);
            
            if(pkt.pts != AV_NOPTS_VALUE)
            {
                outpkt.pts = aud_pts;
//                NSLog(@"%s - %d # ReScaled AUD PTS # 1 # = %lld", __PRETTY_FUNCTION__, __LINE__, outpkt.pts);
            }
            else
            {
                outpkt.pts = AV_NOPTS_VALUE;
            }
            
            if(pkt.dts == AV_NOPTS_VALUE)
            {
                outpkt.dts = AV_NOPTS_VALUE;
            }
            else
            {
                outpkt.dts = aud_pts;
//                NSLog(@"%s - %d # ReScaled AUD DTS # 2 # = %lld", __PRETTY_FUNCTION__, __LINE__, outpkt.dts);
                
                if( outpkt.pts >= outpkt.dts)
                {
                    outpkt.dts = outpkt.pts;
                }
                if(outpkt.dts == aud_dts)
                {
                    outpkt.dts++;
                }
                if(outpkt.pts < outpkt.dts)
                {
                    outpkt.pts = outpkt.dts;
                    aud_pts = (int) outpkt.pts;
                }
            }
            
            outpkt.data = pkt.data;
            outpkt.size = pkt.size;
            outpkt.stream_index = pkt.stream_index;
            outpkt.flags |= AV_PKT_FLAG_KEY;
            vid_pts = aud_pts;
            aud_pts++;
            
            if(av_interleaved_write_frame(outputContext, &outpkt) < 0)
            {
                NSLog(@"%s - %d # Fail Audio Write", __PRETTY_FUNCTION__, __LINE__);
            }
            else
            {
                outputAudioStream->codec->frame_number++;
            }
            
            av_free_packet(&outpkt);
            av_free_packet(&pkt);
        }
        else
        {
            NSLog(@"%s - %d # Got Unknown Pkt", __PRETTY_FUNCTION__, __LINE__);
        }
    }
    
    /*
     *  Write the stream trailer to an output media file and free the file private data.
     *
     *  av_write_trailer(AVFormatContext *s)
     */
    av_write_trailer(outputContext);
    av_free_packet(&outpkt);
    av_free_packet(&pkt);
    
    NSLog(@"%s - %d # Generate Clip completed", __PRETTY_FUNCTION__, __LINE__);
}

@end
